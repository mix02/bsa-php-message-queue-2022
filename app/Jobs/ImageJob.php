<?php

namespace App\Jobs;

use App\Models\User;
use App\Notifications\ImageProcessedNotification;
use App\Services\Contracts\ImageApiService;
use App\Values\Image;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;

class ImageJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(public User $user, public Image $image, public string $filter)
    {
        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ImageApiService $imageApiService)
    {
        
        try {
            $result = $imageApiService->applyFilter($this->image->getSrc(), $this->filter);

            $image = new Image(
                $this->image->getId(),
                $result
            );
            $this->user->notify(new ImageProcessedNotification($image));
        }
        catch(\Exception $e) {
            $this->failed($e);
        }

    }

    public function failed()
    {

    }
}
