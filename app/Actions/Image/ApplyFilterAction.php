<?php

declare(strict_types=1);

namespace App\Actions\Image;

use App\Services\Contracts\ImageApiService;
use App\Actions\Contracts\Response;
use App\Jobs\ImageJob;
use App\Values\Image;
use Illuminate\Support\Facades\Auth;

class ApplyFilterAction
{
    private ImageApiService $imageApiService;

    public function __construct(ImageApiService $imageApiService)
    {
        $this->imageApiService = $imageApiService;
    }

    public function execute(Image $image, string $filter): Response
    {
        $result = $this->imageApiService->applyFilter($image->getSrc(), $filter);

        
        $user = Auth::user();
        dispatch(new ImageJob($user, $image, $filter))->onConnection('beanstalkd');

        return new ApplyFilterResponse(
            new Image(
                $image->getId(),
                $result,
            )
        );
    }
}
